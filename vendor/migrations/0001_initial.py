# Generated by Django 2.2.6 on 2019-10-19 13:52

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Vendor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('info', models.TextField()),
                ('price_level', models.IntegerField(choices=[(1, 1), (2, 2), (3, 3), (4, 4)])),
                ('food_type', models.CharField(max_length=100)),
                ('website', models.URLField(max_length=100)),
                ('slug', models.SlugField()),
                ('email', models.EmailField(max_length=254)),
                ('phone', models.CharField(max_length=100)),
                ('vendor_picture', models.ImageField(upload_to='')),
            ],
        ),
        migrations.CreateModel(
            name='Vendor_Food',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('store_id', models.IntegerField()),
                ('name', models.CharField(max_length=100)),
                ('food_image', models.ImageField(upload_to='')),
                ('store_name', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Vendor_Message',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('store_id', models.IntegerField()),
                ('name', models.CharField(max_length=100)),
                ('message', models.TextField()),
                ('date', models.DateField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Vendor_Review',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('store_id', models.IntegerField()),
                ('name', models.CharField(max_length=100)),
                ('star_ammount', models.IntegerField(choices=[(1, 1), (2, 2), (3, 3), (4, 4), (5, 5)], default=1)),
                ('comment', models.TextField()),
                ('date', models.DateField(auto_now_add=True)),
            ],
        ),
    ]
