from django.test import TestCase, Client


# Create your tests here.

class UrlTest(TestCase):

    def test_home_url(self):
        url = Client().get('/');
        self.assertEqual(url.status_code,200);
