import datetime

from django.test import TestCase, Client

from vendor.models import Vendor_Review


class ReviewModelTest(TestCase):

    def test_zero_input(self):
        size = Vendor_Review.objects.all()
        self.assertEqual(len(size),0)

    def test_add_review(self):
        review = Vendor_Review(
            name="Test",
            store_id=1,
            comment="Test123",
            star_ammount=4
        )
        review.save()

        review_list = Vendor_Review.objects.all()
        review_1 = review_list[0]
        self.assertEqual(len(review_list),1)
        self.assertEqual(review_1.date, datetime.date.today())
        self.assertEqual(review_1.name, "Test")
        self.assertEqual(review_1.store_id, 1)
        self.assertEqual(review_1.comment, "Test123")
        self.assertEqual(review_1.star_ammount, 4)