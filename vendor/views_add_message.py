from django.shortcuts import render, redirect
from vendor.models import Vendor_Message, Vendor

from vendor.message_form import MessageForm

# Create your views here.
def vendor_info(request, slug):
    info = Vendor.objects.get(slug = slug)
    price_lvl = "$"*info.price_level
    
    if (request.method == "POST"):
        forms = MessageForm(request.POST)
        if(forms.is_valid()):
            instance = forms.save(commit=False)
            instance.store_id = info.id
            instance.save()
            return redirect('/vendor/'+slug+'/')
        else:
            return render(request, 'vendor/vendor_info.html', {'forms':forms, 'info':info, 'slug':slug, 'price_level':price_lvl})

    return render(request, 'vendor/vendor_info.html', {'forms':MessageForm(), 'info':info, 'slug':slug, 'price_level':price_lvl})