from django import forms
from django.utils.translation import gettext_lazy as _
from vendor.models import Vendor_Food

class addFoods(forms.ModelForm):
    class Meta:
        model = Vendor_Food
        fields = ['name', 'food_image']
        labels = {
                'name': _("Name"),
                'food_image' : _("Food Image")
                }
        


