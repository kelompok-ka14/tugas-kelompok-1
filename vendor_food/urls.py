from django.urls import path
from vendor_food import views

urlpatterns = [
    path('<str:slug>/', views.addFood)
]