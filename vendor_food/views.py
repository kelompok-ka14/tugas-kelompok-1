from django.shortcuts import render, redirect

from vendor.models import Vendor_Food, Vendor
from vendor_food.forms import addFoods

# Create your views here.
def addFood(request, slug):
    vendor = Vendor.objects.get(slug=slug)
    if (request.method == "POST"):
        forms = addFoods(request.POST, request.FILES)
        if(forms.is_valid()):
            instance = forms.save(commit=False)
            instance.store_id = vendor.id 
            instance.store_name = vendor.name
            instance.save()
            return redirect('/vendor/food/'+slug+"/")
        else:
            foods = Vendor_Food.objects.filter(store_id=vendor.id)
            return render(request, 'vendor_food/foods.html', {'form': forms, 'info': vendor, 'foods':foods})

    foods = Vendor_Food.objects.filter(store_id=vendor.id)
    return render(request, 'vendor_food/foods.html', {'form': addFoods(), 'info': vendor, 'slug':slug, 'foods':foods})