from django import forms
from vendor.models import Vendor

class Vendor_form(forms.ModelForm):
    class Meta:
        model = Vendor
        exclude = ("slug",)