from django.test import TestCase
from django.test.client import Client

class VendorList_URLS_Test(TestCase):
    
    def test_vendor_list_url_exist(self):
        response = Client().get("/vendor_list/")
        self.assertEqual(response.status_code,200)
