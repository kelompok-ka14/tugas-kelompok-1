# from django.test import TestCase
# from django.test.client import Client

# class VendorList_Views_Test(TestCase):
    
#     def test_post_method(self):

#         small_gif = (
#             b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
#             b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
#             b'\x02\x4c\x01\x00\x3b'
#         )
        
#         uploaded = SimpleUploadedFile('small.gif', small_gif, content_type='image/gif')

#         request = {
#             'name':"Name",
#             'info':"abc",
#             'price_level':2,
#             'food_type':"X",
#             'website':"http://website",
#             'email':"x@y.com",
#             'phone':"123",
#             'vendor_picture':uploaded,
#         }

#         response = self.client.post("/vendor/",data=request)
#         objects = Vendor_Review.objects.filter(store_id=1)
#         self.assertEqual(response.status_code,302) #Redirect
#         self.assertEqual(len(objects),1)
#         vendor_creation.delete()

#     def test_wrong_review_input(self):
#         request = {
#             "store_id": 1,
#             "name": "Something",
#             "star_ammount": 4
#         }

#         small_gif = (
#             b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
#             b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
#             b'\x02\x4c\x01\x00\x3b'
#         )
#         uploaded = SimpleUploadedFile('small.gif', small_gif, content_type='image/gif')

#         vendor_creation = Vendor(
#             name="Test Test",
#             info="asdasdasdasdasdasdsad",
#             price_level=2,
#             food_type="Javanese",
#             website="http:// something",
#             email="asdasd@asdasd.com",
#             phone="0123123123",
#             vendor_picture=uploaded,
#         )
#         vendor_creation.save()

#         response = self.client.post("/vendor/review/test-test/",data=request)
#         objects = Vendor_Review.objects.filter(store_id=1)
#         self.assertEqual(response.status_code,200) #If not redirect django rendered the form again
#         self.assertEqual(len(objects),0)
#         vendor_creation.delete()
