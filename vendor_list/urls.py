from django.urls import path, include
from . import views

app_name = "vendor-list"

urlpatterns = [
        path("", views.vendor_list_page, name="vendor-list-page")
]