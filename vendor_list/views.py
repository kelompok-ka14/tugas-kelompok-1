from django.shortcuts import render,redirect
from vendor.models import Vendor
from .form import Vendor_form
# Create your views here.

# def vendor_list_page(request):
#     vendors = Vendor.objects.all()
#     return render(request,"vendor_list/vendor_page.html", {'vendors':vendors, 'counter':range(3)})

def vendor_list_page(request):
    if request.method=="POST":
        form = Vendor_form(request.POST,request.FILES)
        if form.is_valid():
            form.save()
            return redirect("/vendor_list/")
            # vendor = Vendor(
            #     name = form.cleaned_data['name'],
            #     info = form.cleaned_data['info'],
            #     price_level = form.cleaned_data['price_level'],
            #     food_type = form.cleaned_data['food_type'],
            #     website = form.cleaned_data['website'],
            #     email = form.cleaned_data['email'],
            #     phone = form.cleaned_data['phone'],
            #     vendor_picture = form.cleaned_data['vendor_picture'],
            # )
            # vendor.save()
        vendors = Vendor.objects.all()    
        return render(request,"vendor_list/vendor_page.html", {'vendors':vendors, "forms":form}) 
    form = Vendor_form()
    vendors = Vendor.objects.all()    
    return render(request,"vendor_list/vendor_page.html", {'vendors':vendors, "forms":form})
