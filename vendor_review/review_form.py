import django.forms as forms
from django.utils.translation import gettext_lazy as _

from vendor.models import Vendor_Review


class ReviewForm(forms.ModelForm):
      class Meta:
              fields = ['name','star_ammount','comment']
              model = Vendor_Review
              labels = {
                      'name':_("Name"),
                      'star_ammount' : _("Rating"),
                      'comment' : _("Comment")
                      }
              widgets = {
                  'name': forms.TextInput(attrs={
                          'class':'form-control',
                        
                  }),
                        'star_ammount':forms.Select(choices=[(i,i) for i in range(1,5)],attrs={
                                'class':'form-control'
                        }),
                  'comment': forms.Textarea(attrs={
                      'style':'max-height: 200px;',
                      'class':'form-control'
                  })
              }
