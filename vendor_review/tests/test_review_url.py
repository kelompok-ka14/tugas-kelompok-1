from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase, Client

from vendor.models import Vendor


class UrlReviewTest(TestCase):

    def setUp(self):
        self.client = Client()

    def test_post_url(self):
        small_gif = (
            b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
            b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
            b'\x02\x4c\x01\x00\x3b'
        )
        uploaded = SimpleUploadedFile('small.gif', small_gif, content_type='image/gif')

        vendor_creation = Vendor(
            name="Test Test",
            info="asdasdasdasdasdasdsad",
            price_level=2,
            food_type="Javanese",
            website="http:// something",
            email="asdasd@asdasd.com",
            phone="0123123123",
            vendor_picture=uploaded,
        )
        vendor_creation.save()

        status = self.client.post('/vendor/review/test-test/')
        self.assertEqual(status.status_code,200)
        vendor_creation.delete()

    def test_weird_url(self):
        status = self.client.post('/vendors/')
        self.assertEqual(status.status_code,404)

