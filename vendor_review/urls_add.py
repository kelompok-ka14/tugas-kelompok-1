from django.urls import path
from vendor_review import views_add_review

urlpatterns = [
    path('<str:slug>/', views_add_review.add_review)
]