from django.shortcuts import render, redirect

from vendor.models import Vendor_Review, Vendor
from vendor_review.review_form import ReviewForm

# Create your views here.

def add_review(request,slug):
    vendor = Vendor.objects.get(slug=slug)
    if (request.method == "POST"):
        forms = ReviewForm(request.POST)
        if(forms.is_valid()):
            instance = forms.save(commit=False)
            instance.store_id = vendor.id
            instance.save()
            return redirect('/vendor/review/'+slug+"/")
        else:
            reviews = Vendor_Review.objects.filter(store_id=vendor.id)
            return render(request, 'vendor_review/review.html', {'forms': forms, 'reviews':reviews,'info':vendor, 'slug':slug})

    reviews = Vendor_Review.objects.filter(store_id=vendor.id)
    return render(request, 'vendor_review/review.html', {'forms':ReviewForm(),'info':vendor, 'reviews':reviews, 'slug':slug})